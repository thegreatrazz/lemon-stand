require 'rails_helper'

RSpec.describe MarketsController, type: :request do
  describe 'GET /new' do
    it 'responds :success' do
      get new_market_path
      expect(response).to have_http_status(:success)
    end
  end

end


# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Titles', type: :request do
  describe '#index' do
    it 'returns http success' do
      get '/'
      expect(response).to have_http_status(:success)
    end
  end

  describe '#new_game' do
    it 'returns http success' do
      post '/title/new_game'
      expect(response).to have_http_status(:redirect)
    end
  end

  describe '#continue_game' do
    it 'returns http success' do
      post '/title/continue_game'
      expect(response).to have_http_status(:redirect)
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaleService do
  context 'initializes the service with arguments' do
    let(:inventory) { Inventory.new(cash: 5.0, lemons: 1, sugar: 1) }
    let(:market) { Market.new(lemon_price: 0.25, sugar_price: 0.02) }
    let(:subject) { described_class.new(:inventory, :market) }

    it 'gets the current price of lemons and sugar from the market' do
      expect(:subject.market.lemon_price).to eq 0.25
      expect(:subject.market.sugar_price).to eq 0.02
    end

    it 'gets the current lemon, sugar and cash amounts from inventory' do
      expect(:subject.invenory.lemons).to eq 1
      expect(:subject.inventory.sugar).to eq 1
      expect(:subject.inventory.cash).to eq 5.0
    end

    describe '#buy_goods' do
      it 'is a valid transaction' do
        :subject.buy_goods(1, 1)
        expect(subject.inventory.lemons).to eq 2
        expect(subject.inventory.sugar).to eq 2
        expect(subject.inventory.cash).to eq 4.73
      end

      it 'is not a valid transaction' do
        expect { subject.buy_goods(100, 100) }.to raise_error(SaleService::Bankrupt)
      end
    end

  end
end
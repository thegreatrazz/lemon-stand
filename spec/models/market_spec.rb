require 'rails_helper'

RSpec.describe Market, type: :model do
  context 'sets the price for lemons and sugar' do
    let(:subject) { described_class.new(lemon_price: 0.25, sugar_price: 0.02) }
    it 'sets the price between 25 and 50 cents for lemons' do
      expect(subject.lemon_price).to be_between(0.25, 0.5)
    end

    it 'sets the price between 2 and 5 cents for sugar' do
      expect(subject.sugar_price).to be_between(0.02, 0.05)
    end
  end

  context 'lemon price off the range' do
    let(:subject) { described_class.new(lemon_price: 0.2, sugar_price: 0.02) }
    it 'market is invalid' do
      expect(subject.valid?).to be false
    end
  end

  context 'sugar price off the range' do
    let(:subject) { described_class.new(lemon_price: 0.25, sugar_price: 0.10) }
    it 'market is invalid' do
      expect(subject.valid?).to be false
    end
  end

end

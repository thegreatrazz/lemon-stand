require_relative '../rails_helper'

describe DayFactory do
  context 'with a new game' do
    let(:game) { Game.create }
    let(:day) { DayFactory.new_day game }

    it 'is attached to the given game' do
      expect(day.game).to eq game
    end

    it 'has a useful temperature' do
      expect(day.temperature).to be_between(10, 45)
    end

    it 'has a useful population' do
      expect(day.population).to be_between(10, 1000)
    end

    it 'has an undamaged reputation' do
      expect(day.reputation).to be 0.0
    end

    it 'is the first day' do
      expect(day.day_count).to be 1
    end

    it "doesn't have a price set" do
      expect(day.lemonade_price).to be_nil
    end
  end

  context 'with an existing day' do
    let(:game) { Game.create }
    let(:day1) { DayFactory.new_day game }
    let(:day2) { DayFactory.next_day day1 }

    it 'maintains the same game' do
      expect(day2.game).to eq day1.game
    end

    it 'increments the day count' do
      expect(day2.day_count).to be 2
    end

    it 'changes the temperature' do
      expect(day2.temperature).to be_within(20).of(day1.temperature)
    end

    it "doesn't have a price set" do
      expect(day2.lemonade_price).to be_nil
    end

    it 'increases the population' do
      expect(day2.population).to be > day1.population
    end

    it 'keeps the reputation' do
      expect(day2.reputation).to be day1.reputation
    end
  end
end

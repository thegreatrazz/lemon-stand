# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InventoryInterface do
  context 'initialized with no arguments' do
    let(:input) { {} }
    let(:subject) { Inventory.new(**input) }
    let(:inventory) { described_class.new(subject) }

    describe '#copy' do
      it 'creates a new copy of the inventory' do
        subject.update(cash: 20)
        expect(inventory.copy.cash).to eq subject.cash
      end
    end

    describe '#credit' do
      it 'adds cash to the players inventory' do
        inventory.credit(5)
        expect(subject.cash).to eq 10
      end
    end

    describe '#take_lemons' do
      it 'takes lemons from the players inventory' do
        subject.update(lemons: 5)
        inventory.take_lemons(2)
        expect(subject.lemons).to eq 3
      end

      it "raises an error when there aren't enough lemons" do
        expect { inventory.take_lemons 2 }.to raise_error InventoryInterface::NotEnough
      end
    end

    describe '#take_sugar' do
      it 'takes sugar from the players inventory' do
        subject.update(sugar: 5)
        inventory.take_sugar(2)
        expect(subject.sugar).to eq 3
      end

      it "raises an error when there isn't enough sugar" do
        expect { inventory.take_sugar 2 }.to raise_error InventoryInterface::NotEnough
      end
    end

    describe '#make_lemonade' do
      before do
        subject.update(lemons: 1)
        subject.update(sugar: 1)
      end

      it 'makes lemonade with 1 sugar and 1 lemon' do
        inventory.make_lemonade(1)
        expect(subject.lemonades).to eq 1
      end

      it 'if there isnt enough sugar or lemons it will throw an error' do
        expect { inventory.make_lemonade(2) }.to raise_error InventoryInterface::NotEnough
      end

      it 'makes as many lemonades as possible, leaving the remaining lemons' do
        subject.update(lemons: 2)
        inventory.make_lemonade(1)

        expect(subject.lemons).to eq 1
      end

      it 'makes as many lemonades as possible, leaving the remaining sugar' do
        subject.update(sugar: 2)
        inventory.make_lemonade(1)

        expect(subject.sugar).to eq 1
      end
    end

    describe '#take_lemonade' do
      it 'returns the new amount' do
        subject.update(lemonades: 5)
        inventory.take_lemonade(2)
        expect(subject.lemonades).to eq 3
      end

      it "raises an error when there aren't enough lemonades" do
        expect { inventory.take_lemonade 6 }.to raise_error InventoryInterface::NotEnough
      end
    end
  end
end

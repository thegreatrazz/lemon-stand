# frozen_string_literal: true

Rails.application.routes.draw do
  # title screen
  root 'title#index'
  post 'title/new_game'
  post 'title/continue_game'

  #root "markets#new"
  #get "markets", to: "markets#new"
  #  post "markets", to: "markets#create"

  #resources :games
  #resources :days

  resources :inventories
  resources :markets, only: %i[new create]

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

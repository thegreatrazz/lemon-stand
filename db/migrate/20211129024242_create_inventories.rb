# frozen_string_literal: true

# migration for inventory
class CreateInventories < ActiveRecord::Migration[6.1]
  def change
    create_table :inventories do |t|
      t.float :cash, default: 5.0
      t.integer :lemons, default: 0
      t.integer :sugar, default: 0
      t.integer :lemonades, default: 0

      t.timestamps
    end
  end
end

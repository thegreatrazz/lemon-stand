# frozen_string_literal: true

# Adds a Day reference to the Inventories table
class AddDayReferenceToInventory < ActiveRecord::Migration[6.1]
  def change
    add_reference :inventories, :day, null: false, foreign_key: true
  end
end

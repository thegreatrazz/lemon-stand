# Creates the days table in the database
class CreateDays < ActiveRecord::Migration[6.1]
  def change
    create_table :days do |t|
      t.references :game, null: false
      t.integer :day_count, null: false
      t.float :temperature, null: false
      t.float :lemonade_price
      t.integer :population, null: false
      t.float :reputation, null: false

      t.timestamps
    end
  end
end

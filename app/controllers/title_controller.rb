# frozen-string-literals: true

# Presents a title screen to the player and acts as our index page for new visitors.
class TitleController < ApplicationController
  def index
    @game = game_session
  end

  def new_game
    game = Game.new
    day = DayFactory.new_day(game)

    GameSessionStorage.set_game(session, Game.new)
    redirect_to_game
  end

  def continue_game
    redirect_to_game
  end

  private

  def redirect_to_game
    redirect_to markets_path  # TODO: switch over to the day view instead of markets
  end
end

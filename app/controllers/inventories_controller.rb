class InventoriesController < ApplicationController
  def new
    @inventory = Inventory.last
    render(locals: { inventory: Inventory.last })
  end

  def show
    @inventory = Inventory.find(params[:id])
  end

  def update
    @inventory = Inventory.find(params[:id])
    inv = InventoryInterface.new(@inventory)
    val = params[:inventory][:lemonades].to_i
    inv.make_lemonade(val)
    redirect_to @inventory
  end
end

# frozen_string_literal: true

# Manages the player's game session.
module GameSessionStorage
  # Stores the current game instance in the session.
  def self.set_game(session, game)
    session[:game_id] = game.id
  end

  # Retrieves the current game from the session. Returns `nil` if there is no game in the storage.
  def self.get_game(session)
    Game.find(session.fetch(:game_id) { return nil })
  end
end

# Creates validated day objects
class DayFactory
  # Creates a new day object and attaches it to the game.
  def self.new_day(game)
    day = Day.create game: game,
                     day_count: 1,
                     temperature: Random.rand(10.0..45.0),
                     lemonade_price: nil,
                     population: Random.rand(10..100),
                     reputation: 0.0

    Inventory.create day: day
    # TODO: add market

    day.reload
    day
  end

  # Creates a new day object to follow the previous day
  def self.next_day(day)
    original_inventory = day.inventory
    day = Day.create(game: day.game,
                     day_count: day.day_count + 1,
                     temperature: (day.temperature + Random.rand(-10.0..10.0)).clamp(10, 45),
                     lemonade_price: nil,
                     population: day.population + Random.rand(1..10),
                     reputation: day.reputation)

    inventory = InventoryInterface.new(original_inventory).copy
    inventory.day = day
    inventory.save!

    # TODO: add market copy

    day.reload
    day
  end
end
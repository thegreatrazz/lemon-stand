class Market < ApplicationRecord
  validates_inclusion_of :lemon_price, in: 0.25..0.5
  validates_inclusion_of :sugar_price, in: 0.02..0.05
end
